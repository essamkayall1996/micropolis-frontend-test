# Web Application Security Measures

## XSS Protections

- **Sanitization**: Angular's default sanitization prevents unsafe HTML, CSS, and URLs from being injected into the DOM.
- **Data Binding**: Property binding is used to encode and insert dynamic content safely, mitigating the risk of XSS.

## CSRF Protections

- The application exclusively uses `HttpClient.get` requests, which are not susceptible to CSRF attacks.
