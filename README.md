# Frontend Test Application

## Introduction

This Angular 16 application is structured into section-specific folders in the front-end, providing a modular and organized codebase. It is designed to function as a Progressive Web App (PWA) for an enhanced mobile experience.

## Quick Start

To launch the app, run:

```bash
npm run start
```

For testing, execute:

```bash
npm run test
```

## Features

- **Tailwind CSS & Angular Material**: Ensures a responsive and cohesive user interface.
- **Progressive Web App**: Offers a superior mobile experience with PWA features.

## Documentation

For security and performance details, please refer to `SECURITY.md` and `PERFORMANCE.md`.
