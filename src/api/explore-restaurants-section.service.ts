import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs';
import { ApiRoutes, BaseURL } from 'src/constants/api-routes';
import { IFeaturedRestaurant } from '../@types/featured-restaurant.type';
import { ICuisine } from 'src/@types/cuisine-type.type';

@Injectable({
  providedIn: 'root',
})
export class ExplorRestaurantsService {
  constructor(private http: HttpClient) {}

  getCuisineTypes(): Observable<ICuisine[]> {
    return this.http.get<ICuisine[]>(`${BaseURL}/${ApiRoutes.cuisine_types}`);
  }

  searchRestaurants({
    start,
    limit,
    Xaxis,
    Yaxis,
    cuisine_type,
    name_contains,
  }: {
    limit: number;
    start: number;
    cuisine_type?: number | 'all';
    name_contains?: string;
    Xaxis?: number;
    Yaxis?: number;
  }): Observable<any[]> {
    let params = new HttpParams()
      .set('_start', start.toString())
      .set('_limit', limit.toString());

    if (cuisine_type !== 'all') {
      params = params.set(
        'service_provider_types.cuisine_type',
        cuisine_type!.toString()
      );
    }
    if (name_contains) {
      params = params.set('name_contains', name_contains);
    }
    if (Xaxis !== undefined) {
      params = params.set('Xaxis', Xaxis.toString());
    }
    if (Yaxis !== undefined) {
      params = params.set('Yaxis', Yaxis.toString());
    }

    return this.http.get<any[]>(`${BaseURL}/${ApiRoutes.search_restaurants}`, {
      params,
    });
  }

  getRestaurants(): Observable<IFeaturedRestaurant[]> {
    return this.http.get<IFeaturedRestaurant[]>(
      `${BaseURL}/${ApiRoutes.featuered_restaurants}`
    );
  }
}
