import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { ApiRoutes, BaseURL } from 'src/constants/api-routes';
import { IFeaturedRestaurant } from '../@types/featured-restaurant.type';

@Injectable({
  providedIn: 'root',
})
export class FeaturedrestaurantsListService {
  constructor(private http: HttpClient) {}

  getRestaurants(): Observable<IFeaturedRestaurant[]> {
    return this.http.get<IFeaturedRestaurant[]>(
      `${BaseURL}/${ApiRoutes.featuered_restaurants}`
    );
  }
}
