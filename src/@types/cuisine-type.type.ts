export interface ICuisine {
  id: number;
  name: string;
  name_ar: string;
  service_provider_types: ServiceProviderType[];
}

export interface ServiceProviderType {
  id: number;
  service_provider: number;
  cuisine_type: number;
}
