export interface IFeaturedRestaurant {
  category: number;
  id: number;
  name: string;
  name_ar: string;
  type: 'Trending';
  image: Array<{ url: string }>;
}
