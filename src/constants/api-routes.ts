import { environment } from 'src/environment/environment';

export const BaseURL = environment.baseURL;
export const ApiRoutes = Object.freeze({
  featuered_restaurants: 'categories/1/featured',
  cuisine_types: 'cuisine-types',
  search_restaurants: 'service-providers/1/search',
} as const);
