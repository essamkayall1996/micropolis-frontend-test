import { ComponentFixture, TestBed } from '@angular/core/testing';
import { HeroSectionComponent } from './hero-section.component';

describe('HeroSectionComponent', () => {
  let component: HeroSectionComponent;
  let fixture: ComponentFixture<HeroSectionComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [HeroSectionComponent],
    }).compileComponents();

    fixture = TestBed.createComponent(HeroSectionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should have a background image', () => {
    const compiled = fixture.debugElement.nativeElement;
    const div = compiled.querySelector('div');
    expect(div.style.backgroundImage).toContain(
      '../../assets/header-section-img.png'
    );
  });

  it('should display the title "Restaurants"', () => {
    const compiled = fixture.debugElement.nativeElement;
    const title = compiled.querySelector('.text-5xl');
    expect(title.textContent).toContain('Restaurants');
  });

  it('should display the subtitle "Book your table"', () => {
    const compiled = fixture.debugElement.nativeElement;
    const subtitle = compiled.querySelector('.text-lg');
    expect(subtitle.textContent).toContain('Book your table');
  });
});
