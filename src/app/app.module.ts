import { NgModule, isDevMode } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { MatIconModule } from '@angular/material/icon';
import { MatButtonModule } from '@angular/material/button';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ServiceWorkerModule } from '@angular/service-worker';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HeroSectionComponent } from './hero-section/hero-section.component';
import { AdsSectionComponent } from './ads-section/ads-section.component';
import { FeaturedRestaurantsListComponent } from './featured-restaurants-list/featured-restaurants-list.component';
import { ExploreRestaurantsSectionsComponent } from './explore-restaurants-sections/explore-restaurants-sections.component';
import { FooterComponent } from './footer/footer.component';
import { SwiperModule } from 'swiper/angular';
import { HeaderSectionComponent } from './header-section/header-section.component';
import { MatToolbarModule } from '@angular/material/toolbar';
import { HttpClientModule } from '@angular/common/http';
import { MatCardModule } from '@angular/material/card';
import { FormsModule } from '@angular/forms';
import { MatInputModule } from '@angular/material/input';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatDividerModule } from '@angular/material/divider';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { NgxGpAutocompleteModule } from '@angular-magic/ngx-gp-autocomplete';
import { Loader } from '@googlemaps/js-api-loader/';
import { environment } from 'src/environment/environment';
import { InfiniteScrollModule } from 'ngx-infinite-scroll';

@NgModule({
  declarations: [
    AppComponent,
    HeroSectionComponent,
    AdsSectionComponent,
    FeaturedRestaurantsListComponent,
    ExploreRestaurantsSectionsComponent,
    FooterComponent,
    HeaderSectionComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    ServiceWorkerModule.register('ngsw-worker.js', {
      enabled: !isDevMode(),
      // Register the ServiceWorker as soon as the application is stable
      // or after 30 seconds (whichever comes first).
      registrationStrategy: 'registerWhenStable:30000',
    }),
    BrowserAnimationsModule,
    HttpClientModule,
    SwiperModule,
    MatButtonModule,
    MatIconModule,
    MatToolbarModule,
    MatCardModule,
    FormsModule,
    MatFormFieldModule,
    MatInputModule,
    MatDividerModule,
    MatProgressSpinnerModule,
    NgxGpAutocompleteModule,
    InfiniteScrollModule,
  ],
  providers: [
    {
      provide: Loader,
      useValue: new Loader({
        apiKey: environment.google_api_key,
        libraries: ['places'],
      }),
    },
    //...
  ],
  bootstrap: [AppComponent],
})
export class AppModule {}
