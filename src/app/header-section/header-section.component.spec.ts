import { ComponentFixture, TestBed } from '@angular/core/testing';
import { HeaderSectionComponent } from './header-section.component';
import { MatIconModule } from '@angular/material/icon';
import { MatButtonModule } from '@angular/material/button';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

describe('HeaderSectionComponent', () => {
  let component: HeaderSectionComponent;
  let fixture: ComponentFixture<HeaderSectionComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [HeaderSectionComponent],
      imports: [MatIconModule, MatButtonModule, BrowserAnimationsModule],
    }).compileComponents();

    fixture = TestBed.createComponent(HeaderSectionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should display "FRONTEND" and "TEST" in the header', () => {
    const compiled = fixture.debugElement.nativeElement;
    const brandFrontend = compiled.querySelector('.text-lg.opacity-90');
    const brandTest = compiled.querySelector('.text-red-600');
    expect(brandFrontend.textContent).toContain('FRONTEND');
    expect(brandTest.textContent).toContain('TEST');
  });
});
