import { ComponentFixture, TestBed } from '@angular/core/testing';
import {
  HttpClientTestingModule,
  HttpTestingController,
} from '@angular/common/http/testing';
import { FeaturedRestaurantsListComponent } from './featured-restaurants-list.component';
import { SwiperModule } from 'swiper/angular';

describe('FeaturedRestaurantsListComponent', () => {
  let component: FeaturedRestaurantsListComponent;
  let fixture: ComponentFixture<FeaturedRestaurantsListComponent>;
  let httpMock: HttpTestingController;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [FeaturedRestaurantsListComponent],
      imports: [HttpClientTestingModule, SwiperModule],
    });
    httpMock = TestBed.inject(HttpTestingController);
    fixture = TestBed.createComponent(FeaturedRestaurantsListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
