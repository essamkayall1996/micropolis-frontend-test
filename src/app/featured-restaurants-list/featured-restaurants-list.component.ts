import { Component, OnInit, ViewChild } from '@angular/core';
import { IFeaturedRestaurant } from '../../@types/featured-restaurant.type';
import { SwiperComponent } from 'swiper/angular';
import { Autoplay, SwiperOptions } from 'swiper';
import SwiperCore from 'swiper';
import { FeaturedrestaurantsListService } from '@services/featured-restaurants-list.service';

SwiperCore.use([Autoplay]);

@Component({
  selector: 'app-featured-restaurants-list',
  templateUrl: './featured-restaurants-list.component.html',
  styleUrls: ['./featured-restaurants-list.component.css'],
  providers: [FeaturedrestaurantsListService],
})
export class FeaturedRestaurantsListComponent implements OnInit {
  restaurants: IFeaturedRestaurant[] = [];

  constructor(
    private featuredrestaurantsListService: FeaturedrestaurantsListService
  ) {}

  config: SwiperOptions = {
    slidesPerView: 1,
    spaceBetween: 15,
    autoplay: true,
    centeredSlides: true,
    loop: true,
    pagination: {
      clickable: true,
      enabled: true,
    },
    breakpoints: {
      460: {
        slidesPerView: 1,
        pagination: {
          clickable: true,
          enabled: true,
        },
        centeredSlides: true,
      },
      640: {
        slidesPerView: 2,
        pagination: {
          clickable: true,
          enabled: true,
        },
        centeredSlides: true,
      },
      1200: {
        slidesPerView: 5,
        pagination: {
          clickable: true,
          enabled: true,
        },
        centeredSlides: true,
      },
    },
  };

  @ViewChild('swiper', { static: false }) swiper?: SwiperComponent;

  slideNext() {
    this?.swiper?.swiperRef.slideNext(100);
  }

  slidePrev() {
    this?.swiper?.swiperRef.slidePrev(100);
  }

  ngOnInit(): void {
    this.featuredrestaurantsListService
      .getRestaurants()
      .subscribe((restaurants) => {
        this.restaurants = restaurants;
      });
  }
}
