import { ComponentFixture, TestBed } from '@angular/core/testing';
import { FooterComponent } from './footer.component';

describe('FooterComponent', () => {
  let component: FooterComponent;
  let fixture: ComponentFixture<FooterComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [FooterComponent],
    }).compileComponents();

    fixture = TestBed.createComponent(FooterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should render the brand name "FRONTEND TEST"', () => {
    const compiled = fixture.debugElement.nativeElement;
    expect(compiled.querySelector('.text-slate-100').textContent).toContain(
      'FRONTEND'
    );
    expect(compiled.querySelector('.text-red-600').textContent).toContain(
      'TEST'
    );
  });

  it('should render the greeting message', () => {
    const compiled = fixture.debugElement.nativeElement;
    expect(compiled.querySelector('.text-white.text-xs').textContent).toContain(
      "Hey.. what's app ✌️"
    );
  });
});
