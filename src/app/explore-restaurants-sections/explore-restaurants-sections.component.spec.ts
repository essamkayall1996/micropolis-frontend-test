import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ExploreRestaurantsSectionsComponent } from './explore-restaurants-sections.component';
import { ExplorRestaurantsService } from 'src/api/explore-restaurants-section.service';
import {
  HttpTestingController,
  HttpClientTestingModule,
} from '@angular/common/http/testing';
import { MatFormFieldModule } from '@angular/material/form-field';
import { NgxGpAutocompleteModule } from '@angular-magic/ngx-gp-autocomplete';
import { Loader } from '@googlemaps/js-api-loader';
import { MatIconModule } from '@angular/material/icon';
import { MatButtonModule } from '@angular/material/button';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatCardModule } from '@angular/material/card';
import { FormsModule } from '@angular/forms';
import { MatInputModule } from '@angular/material/input';
import { MatDividerModule } from '@angular/material/divider';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { environment } from 'src/environment/environment';
import { InfiniteScrollModule } from 'ngx-infinite-scroll';

describe('ExploreRestaurantsSectionsComponent', () => {
  let component: ExploreRestaurantsSectionsComponent;
  let fixture: ComponentFixture<ExploreRestaurantsSectionsComponent>;
  let service: ExplorRestaurantsService;
  let httpMock: HttpTestingController;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [ExploreRestaurantsSectionsComponent],
      providers: [
        ExplorRestaurantsService,
        {
          provide: Loader,
          useValue: new Loader({
            apiKey: environment.google_api_key,
            libraries: ['places'],
          }),
        },
      ],
      imports: [
        BrowserAnimationsModule,
        HttpClientTestingModule,
        MatFormFieldModule,
        MatIconModule,
        NgxGpAutocompleteModule,
        MatButtonModule,
        MatIconModule,
        MatToolbarModule,
        MatCardModule,
        FormsModule,
        MatFormFieldModule,
        MatInputModule,
        MatDividerModule,
        MatProgressSpinnerModule,
        InfiniteScrollModule,
      ],
    });
    httpMock = TestBed.inject(HttpTestingController);
    service = TestBed.inject(ExplorRestaurantsService);
    fixture = TestBed.createComponent(ExploreRestaurantsSectionsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
