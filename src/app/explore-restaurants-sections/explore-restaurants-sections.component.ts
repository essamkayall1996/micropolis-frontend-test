import { Component, OnInit, ViewChild } from '@angular/core';
import { ICuisine } from 'src/@types/cuisine-type.type';
import {
  BehaviorSubject,
  switchMap,
  debounceTime,
  distinctUntilChanged,
  map,
  finalize,
} from 'rxjs';
import { NgxGpAutocompleteDirective } from '@angular-magic/ngx-gp-autocomplete';
import { ExplorRestaurantsService } from '@services/explore-restaurants-section.service';

@Component({
  selector: 'app-explore-restaurants-sections',
  templateUrl: './explore-restaurants-sections.component.html',
  styleUrls: ['./explore-restaurants-sections.component.css'],
})
export class ExploreRestaurantsSectionsComponent implements OnInit {
  @ViewChild('ngxPlaces') placesRef: NgxGpAutocompleteDirective | undefined;

  cusines: ICuisine[] = [];
  selectedCategories: number | 'all' = 'all';
  searchTerm: string = '';
  isLoading: boolean = true;
  Xaxis: number | undefined = undefined;
  Yaxis: number | undefined = undefined;
  limit: number = 10;
  restaurants: any = [];

  constructor(private explorRestaurantsService: ExplorRestaurantsService) {}

  fetchData = new BehaviorSubject<any>({
    cuisine_type: this.selectedCategories,
    name_contains: this.searchTerm,
    limit: this.limit,
  });

  fetchData$ = this.fetchData.asObservable();

  toggleCategory(index: number | 'all'): void {
    this.selectedCategories = index;
    this.fetchData.next({ cuisine_type: this.selectedCategories });
  }

  onSearchChange(): void {}

  handleAddressChange(place: google.maps.places.PlaceResult) {
    if (place?.geometry?.location?.lat() && place?.geometry?.location?.lng()) {
      this.Xaxis = place?.geometry?.location?.lat();
      this.Yaxis = place?.geometry?.location?.lng();
      this.fetchData.next({
        name_contains: this.searchTerm,
        Xaxis: place?.geometry?.location?.lat(),
        Yaxis: place?.geometry?.location?.lng(),
      });
    }
  }

  searchOnClick(): void {
    this.fetchData.next({ name_contains: this.searchTerm });
  }

  isSelected(index: number | 'all'): boolean {
    return index === this.selectedCategories;
  }

  onScroll() {
    this.limit = this.limit + 10;
    this.fetchData.next({
      limit: this.limit,
    });
  }

  ngOnInit(): void {
    this.fetchData$
      .pipe(
        debounceTime(250),
        distinctUntilChanged(
          (prev, current) =>
            prev.cuisine_type === current.cuisine_type &&
            prev.name_contains === current.name_contains &&
            prev.limit === current.limit
        ),
        switchMap(() => {
          this.isLoading = true;
          return this.explorRestaurantsService
            .searchRestaurants({
              start: 0,
              limit: this.limit,
              cuisine_type: this.selectedCategories,
              name_contains: this.searchTerm,
              Xaxis: this.Xaxis,
              Yaxis: this.Yaxis,
            })
            .pipe(
              finalize(() => {
                this.isLoading = false;
              })
            );
        }),
        map((data) => data.map((item) => item))
      )
      .subscribe((data) => {
        this.restaurants = data;
      });

    this.explorRestaurantsService.getCuisineTypes().subscribe((cusines) => {
      this.cusines = cusines;
    });
  }
}
