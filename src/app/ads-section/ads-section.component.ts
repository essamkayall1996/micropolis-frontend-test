import { Component, ViewChild } from '@angular/core';
import { Autoplay, SwiperOptions } from 'swiper';
import { SwiperComponent } from 'swiper/angular';
import SwiperCore from 'swiper';

SwiperCore.use([Autoplay]);

@Component({
  selector: 'app-ads-section',
  templateUrl: './ads-section.component.html',
})
export class AdsSectionComponent {
  images = [
    '../../assets/swiper/swipper-1.png',
    '../../assets/swiper/swipper-2.png',
    '../../assets/swiper/swipper-3.png',
    '../../assets/swiper/swipper-4.png',
  ];
  config: SwiperOptions = {
    slidesPerView: 1,
    spaceBetween: 5,
    autoplay: true,
    centeredSlides: true,
    loop: true,
    pagination: {
      clickable: true,
      enabled: true,
    },
    breakpoints: {
      460: {
        slidesPerView: 1,
        pagination: {
          clickable: true,
          enabled: true,
        },
        centeredSlides: true,
      },
      640: {
        slidesPerView: 2,
        pagination: {
          clickable: true,
          enabled: true,
        },
        centeredSlides: true,
      },
      1200: {
        slidesPerView: 5,
        pagination: {
          clickable: true,
          enabled: true,
        },
        centeredSlides: true,
      },
    },
  };

  @ViewChild('swiper', { static: false }) swiper?: SwiperComponent;
  slideNext() {
    this?.swiper?.swiperRef.slideNext(100);
  }
  slidePrev() {
    this?.swiper?.swiperRef.slidePrev(100);
  }
}
