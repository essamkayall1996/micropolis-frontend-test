import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AdsSectionComponent } from './ads-section.component';
import { SwiperModule } from 'swiper/angular';

describe('AdsSectionComponent', () => {
  let component: AdsSectionComponent;
  let fixture: ComponentFixture<AdsSectionComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [AdsSectionComponent],
      imports: [SwiperModule],
    });
    fixture = TestBed.createComponent(AdsSectionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
